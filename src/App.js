import logo from './logo.svg';
import React,{ Component } from 'react';
import './App.css';
import Greet from './components/Greet';
import Welcome from './components/Welcome';
import Counter from './components/Counter';


class App extends Component {
  render(){
    return (
          <div className="App">
            <Greet />
            <Welcome/>
            <Counter/>
          </div>
        );
  }
}

export default App;
